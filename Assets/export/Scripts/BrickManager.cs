﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickManager : MonoBehaviour
{
    public GameObject baseBrick;
    [SerializeField] List<GameObject> listOfBrick;
    
    void Start()
    {
        if (listOfBrick.Count < this.transform.childCount)
        {
            foreach (Transform brick in this.transform)
            {
                listOfBrick.Add(brick.gameObject);
            }
        }
    }
    
    public List<GameObject> GetListOfBrick()
    {
        return listOfBrick;
    }
}
