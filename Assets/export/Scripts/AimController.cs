﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;
public class AimController : MonoBehaviour
{
    public float moveSpeed;
    float speed;
    //public float jumpHeight;
    //public float gravity;
    private Player player;
    public float reactivity;
    public float accelTime;
    float accel;
    public float deccelTime;
    public float cooldDown;
     float cooltime;
    float deccel;

    Vector3 dir;
    Vector3 finalDir;
    Vector3 usedDir;
    //Vector3 velocity;
    float x;
    float y;
    //bool grounded;
    bool collided;
    bool sideCollided;
    bool statique;
    //bool jump;
    //float timeJump;
    public GameObject projectile;
    public GameObject launcher;

    public LayerMask collisionLayer;
    public LayerMask playerLayer;
    //public LayerMask groundLayer;
    public GameObject wallHitPart;
    public GameObject playerHitPart;


    public ElevateBricks eb;

    public Text scoreText;
    public int score;
    //public float groundDist;

    private void Start()
    {
        eb = GameObject.FindGameObjectWithTag("brique").GetComponent<ElevateBricks>();
        player = ReInput.players.GetPlayer("Player2");
    }
    public void Update()
    {
        //CheckGround(); 
        CheckCapsuleDirCollision();
        //ApplyGravity();
        /*var dir = */
        GetInput();
        GetSpeed();

        if (!sideCollided && !statique) Move(usedDir);

        if (player.GetButton("Shoot") && cooltime == 0) Shoot();
        if (player.GetButtonDown("Shoot")) Shoot();
        if (cooltime > 0) StartCoolDown();
        scoreText.text = "player 2 score : " + score;
    }
    void GetInput()
    {
        x = player.GetAxisRaw("MoveHorizontalP2");
        y = player.GetAxisRaw("MoveVerticalP2");
        dir = transform.right * x + transform.up * y;
        finalDir = Vector3.Lerp(finalDir, dir, reactivity * Time.deltaTime);
        if (!collided) usedDir = finalDir;

        //return usedDir;
    }
    void GetSpeed()
    {

        if (x == 0 && y == 0)
        {
            //float rate = 1.0f / deccelTime;
            //if (deccel > 0)
            //{
            //    deccel -= Time.deltaTime * rate;
            //}
            speed = Mathf.Lerp(speed, 0, deccelTime * Time.deltaTime);
            if (speed < 0.05f) speed = 0;
        }
        else
        {

            speed = moveSpeed;
        }
    }
    void Move(Vector3 dir)
    {
        if (x != 0 || y != 0)
        {
            float rate = 1.0f / accelTime;

            if (accel < 1)
            {
                accel += Time.deltaTime * rate;
            }
        }

        transform.position += dir * (speed * accel) * Time.deltaTime;
    }
    void CheckCapsuleDirCollision()
    {
        if (collided)
        {
            Ray ray2 = new Ray(transform.position, usedDir);
            RaycastHit hit2;

            if (Physics.Raycast(ray2, out hit2, 0.5f, collisionLayer))
            {
                sideCollided = true;
                //Debug.Log("Side Collision");
                //if (Vector3.Distance(transform.position, new Vector3(hit2.point.x, transform.position.y, hit2.point.z)) < 0.4f)
                //{
                //    transform.position = Vector3.Lerp(transform.position, transform.position + hit2.normal * 0.5f, 25f * Time.deltaTime);
                //}
            }
            //else sideCollided = false;

            Debug.DrawLine(transform.position, transform.position + usedDir);
        }
        else sideCollided = false;
        Ray ray = new Ray(transform.position, finalDir);
        RaycastHit hit;
        //var sPosTop = transform.position + new Vector3(0, -0.25f, 0);
        //var sPosBot = transform.position + new Vector3(0, 0.25f, 0);
        if (Physics.Raycast(ray, out hit, 0.5f, collisionLayer))
        {
            collided = true;
            //Debug.Log("Colllllllide");
            if (Vector3.Distance(transform.position, new Vector3(hit.point.x, transform.position.y, hit.point.z)) < 0.2f)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + hit.normal * 0.2f, 25f * Time.deltaTime);
            }

            if (!sideCollided)
            {

                if (Vector3.Dot(hit.normal, Vector3.up) != 0)
                {
                    //Debug.Log("Hit Ceilling or ground");
                    var dot = Vector3.Dot(finalDir, Vector3.right);
                    if (dot > 0.1f)
                    {
                        //Debug.Log("right");
                        usedDir = Vector3.right;
                        statique = false;
                    }
                    else if (dot < -0.1f)
                    {
                        //Debug.Log("left");
                        usedDir = -Vector3.right;
                        statique = false;
                    }
                    else
                    {
                        usedDir = Vector3.zero;
                        statique = true;
                    }
                }
                if (Vector3.Dot(hit.normal, Vector3.right) != 0)
                {
                    //Debug.Log("Hit Wall");
                    var dot = Vector3.Dot(finalDir, Vector3.up);
                    if (dot > 0.1f)
                    {
                        //Debug.Log("up");
                        usedDir = Vector3.up;
                        statique = false;
                    }
                    else if (dot < -0.1f)
                    {
                        //Debug.Log("^down");
                        usedDir = -Vector3.up;
                        statique = false;
                    }
                    else
                    {
                        usedDir = Vector3.zero;
                        statique = true;
                    }
                }

            }


            //if (Vector3.Dot(finalDir, Vector3.up) > 0) Debug.Log("Up");
            //else if (Vector3.Dot(finalDir, Vector3.up) < 0) Debug.Log("Down");
            //if (Vector3.Dot(finalDir, Vector3.up) > 0) Debug.Log("left");
            //else if (Vector3.Dot(finalDir, Vector3.up) < 0) Debug.Log("right");


        }

        else
        {
            collided = false;
            statique = false;
        }

        Debug.DrawLine(transform.position, transform.position + finalDir * 2f, Color.black);
    }

    void Shoot()
    {
        DetectPlayer();
        GameObject go = Instantiate(projectile) as GameObject;
        go.transform.position = launcher.transform.position;
        go.AddComponent<Projectile>();
        go.GetComponent<Projectile>().SetDir(this.transform.position - launcher.transform.position);
        cooltime = cooldDown;
        Debug.DrawLine(launcher.transform.position,  transform.position - launcher.transform.position, Color.cyan);
    }

    void DetectPlayer()
    {
        Ray ray = new Ray(launcher.transform.position, transform.position - launcher.transform.position);
        RaycastHit hit;
        if (Physics.SphereCast(ray, 0.5f, out hit, 50f, playerLayer))
        {
            if (hit.collider.tag == "Player")
            {
                Debug.Log("Hit Player");
                score++;
                GameObject go = Instantiate(playerHitPart) as GameObject;
                go.transform.position = transform.position;
                
            }
            if (hit.collider.tag == "Surface")
            {
                eb.AddWave(transform.position);
                Debug.Log("Hit Surface");
                GameObject go = Instantiate(wallHitPart) as GameObject;
                go.transform.position = transform.position;
            }
        }
    }

    void StartCoolDown()
    {
        if (cooltime > 0)
        {
            cooltime -= Time.deltaTime;
        }
        if (cooltime <= 0) cooltime = 0;
    }
    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.magenta;

        //Gizmos.DrawWireCube(transform.position - new Vector3(0, 0.5f, 0), new Vector3(0.5f, 0.25f, 0.5f));
    }
}
