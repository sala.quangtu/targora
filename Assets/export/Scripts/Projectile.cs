﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    Vector3 dir;
    float speed = 5f;
    Vector3 baseScale = new Vector3(1.5f, 1.5f, 4);
    Vector3 endScale = new Vector3(0.125f,0.125f,0.5f);
    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = baseScale;
        transform.GetComponent<Renderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += dir * speed * Time.deltaTime;
        transform.localScale = Vector3.Lerp(transform.localScale, endScale, speed * 2 * Time.deltaTime);
        transform.rotation = Quaternion.LookRotation(dir);
        //DetectPlayer();
        if (transform.position.z >= GameObject.FindGameObjectWithTag("Player2").transform.position.z) Destroy(this.gameObject); 
    }

    public void SetDir(Vector3 val)
    {
        dir = val;
    }

    //void DetectPlayer()
    //{
    //    //Ray ray = new Ray(transform.position, dir);
    //    //RaycastHit hit;
    //    //if(Physics.Raycast(ray, out hit, 1f))
    //    //{
    //    //    if(hit.collider.tag == "Player")
    //    //    {
    //    //        Debug.Log("Hit player");
    //    //    }
    //    //    if (hit.collider.tag == "Surface")
    //    //    {
    //    //        Debug.Log("Hit surface");
    //    //        Destroy(transform.gameObject);
    //    //    }
            
    //    //}
    //}
}
