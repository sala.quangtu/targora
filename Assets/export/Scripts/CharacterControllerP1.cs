﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
public class CharacterControllerP1 : MonoBehaviour
{
    public float moveSpeed;
    float speed;
    public float jumpHeight;
    public float gravity;

    public float reactivity;
    public float accelTime;
    float accel;
    public float deccelTime;
    public float bounceForce = 5f;
    float deccel;

    Vector3 dir;
    Vector3 finalDir;
    Vector3 velocity;
    float x;
    bool grounded;
    bool collided;
    bool jump;
    bool propulse;
    bool bounce;
    Vector3 propulseDir;
    Vector3 wallNormal;
    public float propulseForce;
    float timeJump;

    public LayerMask collisionLayer;
    public LayerMask groundLayer;
    public LayerMask bounceLayer;

    public float groundDist;
    int nbwallJump;
    int nbJump;
    Vector3 bounceHit;
    Vector3 bounceDir;
    Vector3 lastFinalDir;

    private Player player;
    private void Start()
    {
        player = ReInput.players.GetPlayer("Player1");
        // Time.timeScale = 0.5f;
    }
    public void Update()
    {
        CheckGround();
        CheckCapsuleDirCollision();
        ApplyGravity();
        var dir = GetInput();
        GetSpeed();

        if (!collided) Move(dir);

        if (player.GetButtonDown("Jump") && jump)
        {
            Jump();
            if (collided) nbwallJump++;
            nbJump++;
        }
        if (propulse) PropulsePlayer(propulseDir);
        ReflectDir();
    }
    Vector3 GetInput()
    {
        x = player.GetAxisRaw("MoveHorizontalP1");

        dir = (transform.right * x).normalized;
        finalDir = Vector3.Lerp(finalDir, dir, reactivity * Time.deltaTime);
        return finalDir;
    }
    void GetSpeed()
    {

        if (x == 0)
        {
            //float rate = 1.0f / deccelTime;
            //if (deccel > 0)
            //{
            //    deccel -= Time.deltaTime * rate;
            //}
            speed = Mathf.Lerp(speed, 0, deccelTime * Time.deltaTime);
            if (speed < 0.05f) speed = 0;
        }
        else
        {

            speed = moveSpeed;
        }
    }
    void ApplyGravity()
    {
        if (!grounded)
        {
            velocity.y += gravity * Time.deltaTime;
            transform.position += velocity * Time.deltaTime;
        }
    }
    void Move(Vector3 dir)
    {
        if (x != 0 || bounce)
        {
            float rate = 1.0f / accelTime;
            if (accel < 1)
            {
                accel += Time.deltaTime * rate;
            }
        }

        transform.position += dir * (speed * accel) * Time.deltaTime;
    }
    void CheckCapsuleDirCollision()
    {
        var sPosTop = transform.position + new Vector3(0, -0.25f, 0);
        var sPosBot = transform.position + new Vector3(0, 0.25f, 0);
        //var dir = new Vector3(finalDir.x, velocity.y, 0);
        if (Physics.CapsuleCast(sPosTop, sPosBot, 0.5f, finalDir.normalized, out RaycastHit hit, 0.25f, collisionLayer))
        {
            if (nbwallJump < 5)
            {
                jump = true;
            }
            wallNormal = hit.normal;
            /*if (walkable) */
            collided = true;
            //Debug.Log("Colllllllide");
            //else collided = false;
            if (Vector3.Distance(transform.position, new Vector3(hit.point.x, transform.position.y, hit.point.z)) < 0.25f)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + hit.normal * 0.24f, 15f * Time.deltaTime);
            }
            //wallTempDir = Vector3.Cross(new Vector3(Mathf.Abs(hit.normal.x), Mathf.Abs(hit.normal.y), Mathf.Abs(hit.normal.z)), Vector3.up);
            //Debug.DrawLine(transform.position, transform.position + temp2 * 3f, Color.yellow);
            return;

        }

        else collided = false;
    }
    void CheckGround()
    {
        //if (!Physics.CheckSphere(transform.position - new Vector3(0, 0.25f, 0), 0.5f, groundLayer))
        //{
        //    //Debug.Log("Not Grounded");
        //    grounded = false;
        //    jump = false;

        //}
        if (!Physics.CheckBox(transform.position - new Vector3(0, 0.5f, 0), new Vector3(0.25f, 0.125f, 0.25f), Quaternion.identity, groundLayer))
        {
            grounded = false;
            if(nbJump > 1)jump = false;
        }
        else
        {
            if (finalDir != transform.right * x) finalDir = transform.right * x;
            propulse = false;
            bounce = false;
            nbJump = 0;
            velocity.y = 0;
            //Debug.Log("6 pied sous terre enculé");
            grounded = true;
            jump = true;
            if (nbwallJump > 0) nbwallJump = 0;
            Ray ray = new Ray(transform.position + new Vector3(0.5f, 0.25f, 0), Vector3.down);
            Ray ray2 = new Ray(transform.position + new Vector3(-0.5f, 0.25f, 0), Vector3.down);
            RaycastHit hit;
            RaycastHit hit2;
            if (Physics.Raycast(ray, out hit, groundDist, groundLayer))
            {
                //Debug.Log("lol");
                if (Vector3.Distance(transform.position, new Vector3(hit.point.x, transform.position.y, hit.point.z)) < groundDist * 0.95f)
                {
                    transform.position = Vector3.Lerp(transform.position, transform.position + hit.normal * groundDist, 25f * Time.deltaTime);
                }
            }
            if (Physics.Raycast(ray2, out hit2, groundDist, groundLayer))
            {
                //Debug.Log("lol2");
                if (Vector3.Distance(transform.position, new Vector3(hit2.point.x, transform.position.y, hit2.point.z)) < groundDist * 0.95f)
                {
                    transform.position = Vector3.Lerp(transform.position, transform.position + hit2.normal * groundDist, 25f * Time.deltaTime);
                }
            }

        }
    }
    void Jump()
    {
        timeJump += Time.deltaTime;
        velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        transform.position += velocity * Time.deltaTime;
        if (collided)
        {
            propulse = true;
            propulseDir = wallNormal + Vector3.up * 0.5f;
            Debug.DrawLine(transform.position, transform.position + finalDir, Color.blue);
        }
        if (timeJump >= 0.1f)
        {
            
            jump = false;
            timeJump = 0;
        }

    }

    void PropulsePlayer(Vector3 dir)
    {
        Debug.DrawLine(transform.position, transform.position + dir * 3f, Color.blue);
        
        finalDir = dir;
        Jump();
        //velocity.y = 0;
        propulse = false;
    }

    void ReflectDir()
    {
        //Vector3 reflectedDir = Vector3.zero;
        lastFinalDir.x = -lastFinalDir.x;
        Ray ray = new Ray(transform.position, new Vector3(finalDir.x, velocity.y, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 0.5f, bounceLayer))
        {
            propulse = true;
            bounce = true;
            bounceHit = hit.point;
            bounceDir = Vector3.Reflect(transform.position + new Vector3(lastFinalDir.x, velocity.y, 0), hit.normal).normalized * bounceForce;
            //bounceDir = Vector3.Cross(refD, Vector3.up);
            var reflectedDir = bounceHit + bounceDir;
            reflectedDir.z = 0;
            PropulsePlayer(reflectedDir);
            Camera.main.GetComponent<scaleUp>().Bumped();
            //hit.collider.GetComponent<bumperAnim>().Bumped();
            //Debug.Log("BOUNCE MODAFUKA");
        }
        else
        {
            lastFinalDir = finalDir;
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        var wantedDir = new Vector3(finalDir.x * 2, velocity.y, 0);
        Gizmos.DrawLine(transform.position, transform.position + wantedDir.normalized * 3f);
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(bounceHit, bounceHit + bounceDir);
        //Gizmos.color = Color.green;
        //var wantedReflect = Vector3.Cross(bounceHit + (bounceDir - bounceHit), Vector3.right);
        //Gizmos.DrawLine(transform.position, bounceHit - wantedReflect);
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(transform.position - new Vector3(0, 0.5f, 0), new Vector3(0.5f, 0.25f, 0.5f));
    }
}
